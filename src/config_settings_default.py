from .config import GeneralSettings, cc_comp_settings, clusty_config,\
network_similarity_settings, clustering_settings


def generate_default_config():
    general = GeneralSettings(
                        work_dir = '.',
                        catalog_file = 'path/to/catalog',
                        station_file = 'path/to/stationfile',
                        st_allowlist = [],  # network.station
                        st_blocklist = [],   # network.station
                        waveform_dir = 'path/to/waveforms',
                        station_subset = {'mindist_stations': 50e3, 'maxdist_stations': 200e3},  # [m]
                        n_workers = 1)

    cc = cc_comp_settings(
                        bp = [3., 0.05, 0.2],
                        downsample_to = 0.1,
                        # twd = [[-2.0, 12.0], [0,20]] # first twd setting of first phase... 
                        # L and R phases need twd setting, was -2 to 180 s
                        pick_path = '',
                        pick_option = 'pyr_file',
                        phase = ['R', 'L'],  # ['P', 'S']
                        components = ['xHZ', 'xHE', 'xHN'],
                        vmodel = 'path/to/velocity-model',
                        compute_arrivals = True,
                        use_precalc_arrivals = False,
                        use_precalc_ccs = False,
                        snr_calc = True,
                        snr_thresh = 2.0,
                        max_dist = 40000.,  # maximum inter event distance [m]
                        debug_mode = False,
                        debug_mode_S = False)

    netsim = network_similarity_settings(
                        method_similarity_computation = 'trimmed_mean_ccs',
                        use_precalc_net_sim = False,
                        trimm_cut = 0.3,
                        get_bool_sta_use=False,
                        apply_cc_station_thresh = True,
                        cc_thresh = 0.7,
                        min_n_stats = 5,
                        az_thresh = 60,
                        combine_components = True,
                        weights_components = [0.4, 0.3, 0.3])

    cl = clustering_settings(
                        method = 'dbscan',
                        dbscan_eps = [0.08,0.12,0.14,0.16,0.18,0.20],
                        dbscan_min_pts = [5],
                        wf_plot = [],  # add a tuple of eps and minpts to get waveform plot, e.g. [0.13, 5]
                        wf_plot_stats = [])  # enter stations for which you want wf plot, slow and optional

    config = clusty_config(
      settings=[general, cc, netsim, cl])

    return config