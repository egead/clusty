import os
import numpy as num
from pyrocko import model
import matplotlib.pyplot as plt
import logging


import glob
from clusty import util_clusty
np = num

logger = logging.getLogger('__name__')

def plot_confu(confu_array):
    fig,ax_arr = plt.subplots()
    ax_arr.imshow(confu_array, vmax=20)
    ax_arr.set_ylabel('reference clusters')
    ax_arr.set_xlabel('harmonized new clusters')
    
    fig.tight_layout()
    plt.show()


def calc_confu(cluster_labels_list1,cluster_labels_list2,
                n_cluster1, n_cluster2, debug=False):

    cluster1 = cluster_labels_list1
    cluster2 = cluster_labels_list2

    confu_array = num.zeros((n_cluster1,n_cluster2))
     
    for i_c in range(len(cluster_labels_list1)):
        confu_array[int(cluster1[i_c]+1),int(cluster2[i_c]+1)] += 1
    if debug:
        plot_confu(confu_array)

    return confu_array


def merge_harmonized_catalogs(cat_list):
    
    cat_names = []
    cats = []
    cluster_labels_list =[]
    cat_merged = []
    n_clusters = []
    confu_dict = {}

    for i_cat, cat_path in enumerate(cat_list):
        fn = os.path.split(cat_path)[-1]
        cat = model.load_events(cat_path, format='yaml')
        cluster_labels = num.asarray([int(ev.extras['cluster_number']) for ev in cat])
        c_label = list(range(-1,max(cluster_labels)+1))
        n_cluster = len(c_label)

        cat_names.append(fn)
        cats.append(cat)
        cluster_labels_list.append(cluster_labels)
        n_clusters.append(n_cluster)

        if i_cat > 0:
            confu_array = calc_confu(cluster_labels_list[0],cluster_labels,
                n_clusters[0], n_cluster)
            confu_dict[i_cat] = confu_array

    ev_add = 0
    ev_rejected = 0
    for i_ev, ev in enumerate(cats[0]):

        cluster_classes = num.array(cluster_labels_list)[:,i_ev]
        cluster_set = sorted(list(set(cluster_classes)))
        
        if len(cluster_set) == 1:
            clust_nr = cluster_classes[0]
            cat_flag = [True, True, True, True]

        elif len(cluster_set) == 2 and -1 in cluster_set:
            clust_nr = cluster_set[-1]
            cat_flag = [False,False,False,False]

            for i in np.where(cluster_classes == clust_nr)[0]:
                cat_flag[int(i)] = True

            if cat_flag[0] == False:
                ev_add += 1
            
        else:
            cluster_classes_temp = cluster_classes.copy()

            prim_idx = num.where(cluster_classes > -1)[0][0]
            prim_label = cluster_classes[prim_idx]

            for i_cat in range(len(cluster_classes))[prim_idx+1:]:

                if cluster_classes[i_cat] == -1:
                    continue

                sec_label = cluster_classes[i_cat]
                confu_array = confu_dict[i_cat]

                idx = confu_array[:,sec_label+1].argsort()[::-1]
                cluster_corrected = num.array(confu_array[:,sec_label+1].argsort()[::-1])-1
                common_events = confu_array[:,sec_label+1][idx]

                logger.debug('%i vs. %i' % (prim_label,sec_label))
                logger.debug(confu_array[:,sec_label+1])
                logger.debug(cluster_corrected)
                logger.debug(common_events)

                if cluster_corrected[0] == prim_label:
                    cluster_classes_temp[i_cat] = prim_label

                elif cluster_corrected[0] == -1 and cluster_corrected[1] == prim_label: 
                    cluster_classes_temp[i_cat] = prim_label  

            
            cluster_set_temp =  sorted(list(set(cluster_classes_temp)))

            if len(cluster_set_temp) == 1:
                clust_nr = cluster_classes_temp[0]
                cat_flag = [True]*4

                logger.debug('added', cluster_classes,cluster_classes_temp,clust_nr)


            elif len(cluster_set_temp) == 2 and -1 in cluster_set_temp:
                clust_nr = cluster_set_temp[-1]
                cat_flag = [False]*4

                for i in np.where(cluster_classes == clust_nr)[0]:
                    cat_flag[int(i)] = True

                if cat_flag[0] == False:
                    ev_add += 1

                logger.debug('added', cluster_classes,cluster_classes_temp,clust_nr)

            else:
                clust_nr = -1
                cat_flag = [False]*4

                if cat_flag[0] == False:
                    ev_rejected += 1

                logger.debug('exclude %s %s %s' % (cluster_classes,cluster_classes_temp,clust_nr))
        
        ev.extras['cluster_number'] = clust_nr
        ev.extras['color'] =  util_clusty.cluster_to_color(clust_nr)
        ev.extras['origins'] = cat_flag
        cat_merged.append(ev)
    
    n_clustered = sum(ev.extras['cluster_number'] != -1 for ev in cat_merged)
    n_above_thresh = sum(ev.extras['clustered'] is True for ev in cat_merged)

    logger.info('%i of %i events above thresholds clustered, total %i events in catalog' % (n_clustered, n_above_thresh, len(cat_merged)))
    logger.info('%i events added to reference clustering result' % ev_add)
    logger.info('%i events rejected due to ambiguous cluster associations' % ev_rejected)

    return cat_merged

