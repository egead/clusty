import sys

from setuptools import setup, find_packages


version = '0.1'


setup(
    name='clusty',
    version=version,
    author='clusty collective',
    install_requires=['scikit-learn>=0.21.3'],
    packages=['clusty'],
    package_dir={'clusty': 'src'},
    entry_points={
        'console_scripts': [
            'clusty = clusty.clusty:main',
        ]
    }
)
