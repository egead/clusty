import numpy as np
import matplotlib.pyplot as plt
from pyrocko import model

import matplotlib.patches as patches
import collections

from matplotlib.colors import LinearSegmentedColormap

from clusty import util_clusty
num = np


nstats_file = '/media/peter/6A0B407F2CBF4F4E/greece_clusty/clusty_rundir/trimmed_mean_fivestat_0.05-0.2Hz/precalc_arrays/n_used_stats.npy'
sta_use_file = '/media/peter/6A0B407F2CBF4F4E/greece_clusty/clusty_rundir/005_allarrays/00502/precalc_arrays/sta_use.npy'
catalog= '/home/peter/Desktop/clllusty/cat_trimmed_mean_ccs_dbscan_comb_0.13_5.yaml'

# netsim1 = np.load('/media/peter/6A0B407F2CBF4F4E/greece_clusty/clusty_rundir/005_allarrays/00502/precalc_arrays/_network_similarity.npy')
# netsim2 = np.load('/media/peter/6A0B407F2CBF4F4E/greece_clusty/clusty_rundir/005_allarrays/00502/precalc_arrays/network_similarity.npy')


# fig3, axi = plt.subplots(2,3)

# axi[0,0].imshow(netsim1[0])
# axi[1,0].imshow(netsim2[0])
# axi[0,1].imshow(netsim1[1])
# axi[1,1].imshow(netsim2[1])
# axi[0,2].imshow(netsim1[2])
# axi[1,2].imshow(netsim2[2])
# plt.show()

cat = model.load_events(catalog)
n_stats = np.load(nstats_file)[0]

sta_use = np.load(sta_use_file)
sta_shape= np.shape(sta_use)

stations = model.load_stations('/media/peter/6A0B407F2CBF4F4E/greece_clusty/clusty_rundir/005_allarrays/00502/used_stations.pf')

id_cl = np.array([i for i, ev in enumerate(cat) if ev.extras['cluster_number'] != -1])
cluster_number = np.array([ev.extras['cluster_number'] for i, ev in enumerate(cat) if ev.extras['cluster_number'] != -1])
cluster_number_all = np.array([ev.extras['cluster_number'] for i, ev in enumerate(cat)])

counter = collections.Counter(cluster_number)
ordered_counter = collections.OrderedDict(sorted(counter.items()))

new_stats = np.zeros((len(id_cl),len(id_cl)))

#compatibilty, catching bug in former clusty version
n_stats[n_stats == 32] = 0

id_cl = id_cl[np.argsort(cluster_number)]

for ind_cl1, ind_old1 in enumerate(id_cl):
    for ind_cl2, ind_old2 in enumerate(id_cl):
        if cat[ind_old1].extras['cluster_number'] == cat[ind_old2].extras['cluster_number']:
            new_stats[ind_cl1,ind_cl2] = n_stats[ind_old1,ind_old2]

new_stats += new_stats.T
fig,ax = plt.subplots(figsize=(10,10))

# bins = list(np.arange(1,np.max(new_stats)+1,1))
# hist, bin_edges = np.histogram(new_stats, bins=bins)

# ax[0].bar(bin_edges[:-1],hist)
# ax[0].set_yscale('log')

cdict1 = {'red':   [(0.0,  0.8, 0.8),
                        (1.0,  0.0, 0.0)],
    
             'green': [(0.0,  0.8, 0.8),
                       (1.0,  0.0, 0.0)],
    
             'blue':  [(0.0, 0.8, 0.8),
                      (1.0,  0.0, 0.0)]}

grey2 = LinearSegmentedColormap('Greys2', cdict1)

mycolors = util_clusty.get_colormap()

new_stats[new_stats == 0] = np.nan


means = []
maxs = []
colors = []
cluster_numbers = []
sizes = []

k=0


use_counter = np.zeros((3,len(stations),len(range(max(cluster_number)+1))))
use_counter_all = np.zeros((3,len(stations)))

fig_count,ax_count = plt.subplots()

ev_nr = []
pair_nr = []


for i_comp in range(3):
    #print('component', i_comp)
    for i_sta,sta in enumerate(stations):
        use_counter_all[i_comp,i_sta] = np.sum(sta_use[i_comp,i_sta,:,:])


print(ordered_counter.items())
for clu, n in ordered_counter.items():
    rect = patches.Rectangle((k-0.5,k-0.5),n,n,linewidth=1, edgecolor='none',facecolor=mycolors[clu+1],zorder=0)
    ax.add_patch(rect)

    idx_ev = np.where(cluster_number_all == clu)[0]
    print(idx_ev)
    for i_comp in range(3):
        #print('component', i_comp)
        for i_sta,sta in enumerate(stations):
            for i_ev1 in idx_ev:
                for i_ev2 in idx_ev:
                    use_counter[i_comp,i_sta,clu] += sta_use[i_comp,i_sta,i_ev1,i_ev2]
        
    idx_start = k
    idx_end = k+n

    sliced = new_stats[idx_start:idx_end,idx_start:idx_end]
    flatten_cluster = sliced.flatten()

    sta_mean = np.nanmean(sliced)
    sta_max = np.nanmax(sliced)

    #print('Cluster %i: %.2f / %.2f' % (clu, sta_mean, sta_median))
    means.append(sta_mean)
    maxs.append(sta_max)
    colors.append(mycolors[clu+1])
    cluster_numbers.append(clu)
    sizes.append(n)

    k += n

    ev_nr.append(n)
    pair_nr.append(np.sum(use_counter[0,:,clu]))

    print('----%i (%i)---' % (clu,n))
    print(use_counter[0,:,clu], np.sum(use_counter[0,:,clu]))
    #print(np.array([i for i, ev in enumerate(cat) if ev.extras['cluster_number'] == clu]))
    #print('-----------------------')

print(len(ev_nr),len(pair_nr))

print('all', use_counter_all[0])
ax_count.scatter(ev_nr,pair_nr, c=colors)
plt.show()
sta_contribution = np.zeros_like(stations)
for i_sta, sta in enumerate(stations):
    sta_contribution[i_sta] = np.sum(use_counter[0,i_sta,:])
    print('----%s.%s: %i' % (sta.network,sta.station,np.sum(use_counter[0,i_sta,:])))

#print(sta_contribution)

sizes = np.array(sizes)
fig2,ax2 = plt.subplots()

ax2.scatter(cluster_numbers, maxs, color=colors, marker='^', label='maximum', s=sizes)
ax2.scatter(cluster_numbers, means, color=colors, marker='o', label='mean', s=sizes)

# ax3=ax2.twinx()

# ax3.plot(cluster_numbers,sizes, alpha=0.6)
# ax3.set_ylim(0,300)
# ax3.set_ylabel('Cluster size')
ax2.set_ylabel('Contributing stations per cluster')
ax2.set_xlabel('Cluster label')

ax2.legend()

nst = ax.imshow(new_stats, origin='lower', cmap=grey2, vmin=5, zorder=1)
cbar = fig.colorbar(nst)
ax.set_title('Number of contributing stations')

ax.set_ylabel('Events sorted by cluster')
ax.set_xlabel('Events sorted by cluster')
fig.savefig('/home/peter/Desktop/station_cluster_contrib.pdf')
fig2.savefig('/home/peter/Desktop/contrib_statistics.pdf')

plt.show()