# cross correlation
def corr_cal(iterable):
        coef_row, weights_row = [], []
        for j in range(len(traces_aspo)):

            if j < iterable:
                coef = 0
                weighting_factor = 0
                c = np.nan

            elif iterable == j:
                coef, c = 1, np.nan

            else:

                a = traces_aspo[iterable]
                b = traces_aspo[j]
                if a != 'NaN' and b != 'NaN':
                    # mode = full --> process traces with gap between each other
                    c = trace.correlate(a, b, mode = 'full', normalization= 'normal')
                    c_abs = trace(ydata=num.abs(c.ydata))
                    tpeaks, apeaks = c_abs.peaks(threshold=0.1, tsearch=0.3)
                    # get first and second maximum of absolute cc trace,
                    # get non-abs value of these maxima!!


                    #weighting_factor = s[1]-s[0]

                else:
                    coef = 0

            #cor_row.append(c) im moment nicht
            coef_row.append(coef)
            weights_row.append(weighting_factor)

        if iterable%10 == 0:
            print 'Event ' + str(iterable) + '/' + str(len(traces_aspo)-1)

        return coef_row, weights_row



# routine start

time_list = [ev.time for ev in catalog]

for i_sta, station in enumerate(stations):
    ######all stages in a single run#####
    ### create a pile containing multiple traces ###
    p = pile.make_pile(str(data_path) + '/' + str(station.name), fileformat = 'yaff')

    print('Extracting event traces...')

    traces_aspo=[]

    for index,t in enumerate(time_list):

        for trace0 in p.chopper(tmin = tmin-0.001, tmax = tmax+0.001):
            #count = count+1
            for tr in trace0:
                #tr.deltat=0.000001
                #tr.downsample_to(0.00001, allow_upsample_max=4)
                tr.bandpass(4,fmin,fmax)
                tr.set_channel('SHZ')
                tr.set_station(station.name)
                tr.set_location('TASN')
                tr.set_network('ASPO')
                #calculate SNR
                #tr.chop(tmin = tmin-0.001, tmax=tmax)
                #cut_win = 0.0005-cut_lim[0]
                #snr[i_sta,index,ph_idx,:] = aspo.SNR(tr.ydata, cut_win)
                tr.chop(tmin = tmin, tmax = tmax)

            traces_aspo.append(tr)

        if index%10 == 0:
            print 'Event ' + str(index) + '/' + str(len(catalog)-1) + ' cut'

    print(str(len(traces_aspo)) + ' events in given continuous waveforms')


    ###########PARALLEL###############

    # establish ProcessPool, return results from corr_cal via map function (iterating over each event)
    with ProcessPoolExecutor(max_workers=cores) as executor:
        #print (executor.map(corr_cal, range(len(traces_aspo)))).result() # returns an iterable
        results = executor.map(corr_cal, range(len(traces_aspo))) # returns an iterable

    cor_mat1, lag_time1, cor1 = zip(*results)

    #get specific values for coefficient and time lag
    # cor_mat1 = map(operator.itemgetter(0),results)
    # lag_time1 = map(operator.itemgetter(1),results)
    # cor1 = map(operator.itemgetter(2),results)

    # add correlation coeeficients (symetric for correlation coefficient, lag time negative!)
    A = np.array(cor_mat1)
    cor_mat = A + A.T - np.diag(np.diag(A))

    B = np.array(lag_time1)
    lag_time = B + np.negative(B.T) -np.diag(np.diag(B))

    C = np.array(cor1)
